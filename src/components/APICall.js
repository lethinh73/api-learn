import axios from "axios";
import React, { useState } from "react";

const apiURL = "https://disease.sh/v3/covid-19/countries/usa";

function APICall() {
  const [post, setPost] = React.useState(null);

  useEffect(() => {
    axios.get(apiURL).then((response) => {
      setPost(response.data);
    });
  }, []);

  if (!post) return null;

  return post;
}

export default APICall;
