import React from "react";
import APICall from "./APICall";
import "../styles/App.css";

function numberFormatter(num) {
  const formatter = new Intl.NumberFormat().format(num);
  return formatter;
}

function App() {
  const data = new APICall();
  const cases = numberFormatter(data.cases);
  const deaths = numberFormatter(data.deaths);
  const casesPerCapita = numberFormatter(data.casesPerOneMillion);

  const date = new Date();
  const mm = date.getMonth().toString();
  const dd = date.getDay().toString();
  const yyyy = date.getFullYear().toString();
  const today = mm + "/" + dd + "/" + yyyy;

  return (
    <div className="container-lg">
      <div className="row">
        <h2>COVID-19 in the United States</h2>
      </div>
      <div className="row">
        <h3>Latest data from {today}</h3>
      </div>
      <div className="row">
        <div className="col">
          <h5>Confirmed Cases</h5>
          <h3>{cases}</h3>
          <h8>In the USA</h8>
        </div>

        <div className="col">
          <h5>Cases per Capita</h5>
          <h3>{casesPerCapita}</h3>
          <h8>per 1,000,000 people</h8>
        </div>

        <div className="col">
          <h5>Deaths</h5>
          <h3>{deaths}</h3>
          <h8>In the USA</h8>
        </div>
      </div>
    </div>
  );
}

export default App;
